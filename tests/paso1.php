<?php
use PHPUnit\Framework\TestCase;

class CharacterTest extends TestCase
{
    public function testCharacterGetInitialValues()
    {
        $character = new Character();

        $this->assertEquals(1000, $character->getHealth());
        $this->assertEquals(1, $character->getLevel());
        $this->assertEquals(true, $character->isAlive());
    }

}

class Character
{
    public function getHealth()
    {
        return 1000;
    }
    public function getLevel()
    {
        return 1;
    }
    public function isAlive()
    {
        return true;
    }
}
