<?php
use PHPUnit\Framework\TestCase;

class CharacterTest extends TestCase
{
    public function testCharacterGetInitialValues()
    {
        $character = new Character();

        $this->assertEquals(1000, $character->getHealth());
        $this->assertEquals(1, $character->getLevel());
        $this->assertEquals(true, $character->isAlive());
    }

    public function testCanDamageOtherCharacter()
    {
        $hitter = new Character();
        $receiver = new Character();

        $hitter->damage($receiver, 350);

        $this->assertEquals(650, $receiver->getHealth());
        $this->assertEquals(true, $receiver->isAlive());

        // se añade isAlive, para diferenciarlo del otro test
    }

    public function testIfReceivesMoreDamageThanHealthCharacterDies()
    {
        $hitter = new Character();
        $receiver = new Character();
        $this->assertEquals(1000, $receiver->getHealth());

        $hitter->damage($receiver, 1350);

        $this->assertEquals(0, $receiver->getHealth());
        $this->assertEquals(false, $receiver->isAlive());
    }

}

class Character
{
    private $health;

    public function __construct()
    {
        $this->health = 1000;
    }
    public function getHealth()
    {
        return $this->health;
    }
    public function getLevel()
    {
        return 1;
    }
    public function isAlive()
    {
        return $this->health > 0;
    }
    public function damage($to, $damage)
    {
        $to->health -= $damage;
        if ($to->health <= 0) {
            $to->health = 0;
        }
    }
}
