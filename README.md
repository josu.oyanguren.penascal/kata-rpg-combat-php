PHP kata boilerplate
====================

Requisitos
----------

- PHP 7.2+ (`sudo apt install php`)
- composer (`sudo apt install composer`)

Instalación
-----------

1. Copiar siempre esta carpeta (esta carpeta no debe modificarse,
   para que se pueda utilizar como plantilla de otros proyectos).

2. Instalar las dependencias

    `$ composer install`

3. Modificar los archivos `src/Sut.php` y `tests/SutTest.php`,
    la clase `Sut` en `src/Sut.php` y las referencias a esa clase en `tests/SutTest.php`
    por un nombre con más significado.

4. Ejecutar los tests existentes para comprobar que todo va bien

    `./vendor/bin/phpunit tests/`

    Deberías ver una salida similar a esta:

        PHPUnit 8.5.4 by Sebastian Bergmann and contributors.

        .FI                                                                 3 / 3 (100%)

        Time: 30 ms, Memory: 4.00 MB

        There was 1 failure:

        1) SutTest::testIsFalse
        Failed asserting that true matches expected false.

        /home/josu/apps/f5/katas/php-katas/000-boilerplate/tests/SutTest.php:16

        FAILURES!
        Tests: 3, Assertions: 2, Failures: 1, Incomplete: 1.


## Consideraciones

-   Los archivos `./composer.json` y `./phpunit.xml` son los archivos
    configuración del proyecto, manejarlos con precaución.

-   Se ha dejado un archivo `./.vscode/tasks.json` con una tareas definidas

    `test` - ejecuta los tests, está configurada como "Tarea de compìlación
    predeterminada" y se ejecuta directamente con `Terminal - Ejecutar compilar tarea ...`.

    **Se recomienda establecer un atajo de teclado sencillo para lanzar `test`.**
